
























April 22, 2016 
 
 
 
 

Secretary Sally Jewell 
U.S. Department of the Interior 
Director (630) 
Bureau of Land Management 
2134 LM, 1849 C St. NW 
Washington, DC 20240 
Attention: 1004-AE14 
 
 
RE: Docket ID: BLM-2016-0001-0001 
 
 
Dear Secretary Jewell, 
 
I write to urge you to withdraw the Bureau of Land Management’s proposed Waste Prevention, 
Production Subject to Royalties, and Resource Conservation rule. This rule stands to harm my 
local community, state, and our nation.   
 
Fossil fuels are currently the life blood of America’s economy and will be for at least the next 50 
years. My state currently relies on oil and gas operations for energy supplies, royalty and tax 
revenues, to make everyday household chemicals, to grow and produce food, and to keep costs 
low at both the pump and at the furnace. As it is written, the proposed rule will force numerous 
oil and gas operators to shut down wells prematurely. If this happens, it will have devastating 
impacts on every industry and sector of my community and my state’s economy.   
 
I again urge you to withdraw the BLM’s rule and to work with local and state experts to find a 
solution that achieves your intended result without harming my community.   
 
Thank you, 
 
 
P Smith 
Oxford, AL 
 
CC: BLM Director Neil Kornze



