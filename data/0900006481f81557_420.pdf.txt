
























Neil Kornze 
BLM Director
 
I’m writing to thank you for the administration's actions to lower methane waste and
pollution from the oil and gas industry on our public lands. To protect our climate from
harmful methane pollution, we need to stop venting and flaring, and the Bureau of Land
Management's draft rules are an important step forward.
 
It is great to see the BLM cover existing oil and gas infrastructure under these new
standards -- a critical component needed to reduce methane pollution and protect public
health. As the administration finalizes these rules, I urge you to make some improvements
to ensure that public lands are protected and gas is not wasted:
 
Improve leak detection and repair requirements: Increase the frequency of required leak
inspections to quarterly for all wells. Set a protective definition of what constitutes a leak,
such as 500 ppm for those operators using a quantitative leak detection method. Set
conservative parameters around the proposed alternative compliance programs so that
they incentivize technological advances, but do not create loopholes for weaker inspection
programs. This would more closely align the proposal with what leading states like
Colorado and Wyoming require and would capture more leaks and waste. The rules
should be improved to:
 
Better regulating all aspects of production: Strengthen the control measures and
monitoring requirements applicable to pneumatic controllers by requiring the use of zero-
bleed pneumatics where feasible and extending standards to intermittent-bleed (which
account for 85 percent of production emissions nationwide according to industry reporting),
as well as continuous bleed natural gas-driven pneumatic controllers, as the state of
Wyoming has done. Require rigorous monitoring of emissions from pneumatic controllers
to ensure these devices are operating properly.
 
Improve flaring regulation: To more effectively limit the waste of federal and tribal natural
gas resources due to flaring: 1) make gas capture plans mandatory and binding, 2) tighten
exemption provisions by limiting the scope of current exemptions, their length, and the
ability to renew exemptions indefinitely, and 3) require operators to report all capture and
forgone revenue information in applications. Set a time limit beyond which flaring, even
below 1,800 MCF per month per well, is prohibited.
 
Proper enforcement: BLM needs to ensure enforcement of the venting, flaring, and leak
regulations. Enforcement of waste minimization plans is also crucial to ensuring that the
plans are carried through during development and production of oil and gas on federal
lands.
 
Ensure transparency and accountability: BLM should improve the rule to provide for
improved transparency and accountability. To do this, BLM should provide for public
involvement in: (1) proposed alternative flaring limits, (2) requests for a 2-year renewable
exemption from the flaring limit, (3) state/tribal requests for a variance, and (4) thorough
agency reviews of applications for permits to drill and operator-submitted waste
minimization plans.



