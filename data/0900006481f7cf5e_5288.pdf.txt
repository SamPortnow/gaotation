
























U.S.	  Department	  of	  the	  Interior	  
Director	  Bureau	  of	  Land	  Management	  
20	  M	  St.	  SE	  
Room	  2134LM	  
Washington	  DC	  20003	  
	  
Attention:	  	  1004-­‐AE14	  
	  
Oil	  and	  natural	  gas	  production	  on	  BLM-­‐managed	  lands	  has	  helped	  reduce	  energy	  costs	  for	  
American	  families	  and	  enhanced	  our	  nation’s	  energy	  security.	  	  Government	  overregulation	  of	  
methane	  emissions	  from	  these	  wells	  will	  negatively	  impact	  our	  economy,	  make	  us	  more	  
dependent	  on	  foreign	  sources	  of	  energy,	  and	  is	  totally	  unnecessary.	  
	  
Improvements	  in	  technology	  have	  already	  significantly	  reduced	  methane	  emissions	  from	  BLM-­‐
based	  wells	  –	  allowing	  us	  to	  protect	  our	  environment	  while	  developing	  these	  strategically	  
important	  resources.	  	  	  
	  
I	  urge	  you	  to	  reject	  these	  unnecessary,	  and	  risky,	  regulations.	  
	  
Sincerely	  
	  
Alice	  Fitzgerald	  
1123	  Nalder	  St	  
Layton,	  UT	  84040	  
	  



