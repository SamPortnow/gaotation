
























Ryan Zinke 
Interior
 
Dear Secretary Zinke,
 
I urge the Bureau of Land Management not to rescind your 2015 rule addressing hydraulic
fracturing on federal and Indian lands, 82 Fed. Reg. 34,464 (July 25, 2017). If BLM
chooses to revisit the 2015 Rule, the agency should consider strengthening its
requirements–not repealing them.
 
Sincerely,
 
Susan Hittel
250 W 90th Street Apt 10-H
Apt 10-H
Manhattan New York, NY 10024
 
2123561420



