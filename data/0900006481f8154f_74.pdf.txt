
























 
In addition to strong standards to curb methane pollution and waste, we must work to keep
as many fossil fuels in the ground as possible if we are going to stave off the worst effects
of climate change. Methane is a highly potent greenhouse gas that we cannot continue to
emit if we want to meet the administration's international climate commitments. It’s time to
end oil and gas leasing on public lands.
 
Julie Jewelers
 
Miami, FL 33137



