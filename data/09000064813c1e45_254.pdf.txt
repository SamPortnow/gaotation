
























Brent Gurtek 
1873 Korkki Rd 
Duluth, MN 55804-9638 
(218) 525-7573 
 

Aug 7, 2013 
 
President Barack Obama 
The White House, 1st Floor, West Wing 
1600 Pennsylvania Avenue, NW 
Washington, DC 20500 
 
Subject: Oil and Gas; Hydraulic Fracturing on Federal and Indian Lands 
 
Bureau of Land Management: President Obama, 
 
Comment: Document ID BLM-2013-0002-0011 
 
I am calling for a ban on fracking on all federal lands. This land is our land and should be managed for the 
good of the people, not corporate profits for the oil and gas industry. The proposed rules for hydraulic 
fracturing on Federal and Indian lands are too weak. The best way to protect our air, water, wildlife, 
climate and public health is simply to prohibit this inherently dangerous form of fossil fuel extraction. 
 
The Bureau of Land Management's proposed rules for regulating hydraulic 
fracturing on Federal and Indian lands are not only weak, they do not 
take into account all processes required to frack for oil and gas. 
 
Fracking, defined as the combined process of drilling, fracturing, 
processing, disposing of waste and transporting oil and gas, has not 
been proven safe. 
 
The agency's mission is "to sustain the health, diversity, and 
productivity of America's public lands for the use and enjoyment of 
present and future generations." Fracking conflicts with this 
mission. 
 
I urge you: don't frack on federal public and Indian lands!  
 
Sincerely, 
Brent Gurtek 
 



