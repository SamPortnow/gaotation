
























Ryan Zinke 
Interior
 
Dear Secretary Zinke,
 
I urge the Bureau of Land Management not to rescind your 2015 rule addressing hydraulic
fracturing on federal and Indian lands, 82 Fed. Reg. 34,464 (July 25, 2017). If BLM
chooses to revisit the 2015 Rule, the agency should consider strengthening its
requirements–not repealing them.
 
Sincerely,
 
Vincent Petta
308 Oak Track Crse
Ocala, FL 34472
 
3526800822



