
























Sally Jewell 
Department of the Interior
 
Dear Secretary Jewell,
 
Time is running out to take meaningful action on the flaring and venting of natural gas on
public and tribal lands. I am concerned that the proposed rule would still allow
considerable royalty-free venting and flaring on public and tribal lands. The clock is ticking
on our climate. We cannot allow Big Oil to continue to burn fossil fuels royalty-free.
 
Sincerely,
 
Guy Russo
13200 Sentinal CT NE
Albuquerque, NM 87047



